import React from 'react';
class CustomComponent1 extends React.Component {
  render() {
    return (
      <div>Custom Component 1!</div>
    );
  }
}

class CustomComponent2 extends React.Component {
  render() {
    return (
      <div>Custom Component 2!</div>
    );
  }
}

export { CustomComponent1, CustomComponent2};
